title: "XXX"
perex: "XXX"
authors: ["Jan Boček, Alžběta Nečasová"]
published: "19. července 2017"
coverimg: https://samizdat.cz/data/slovaci-v-cesku/media/sp.png
coverimg_note: ""
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1", "https://code.highcharts.com/highcharts.js"]
options: "" #wide
---

Necelých deset let po rozdělení Československa to vypadalo, že Češi a Slováci se rozešli nejen papírově, ale i fakticky. Na přelomu století žilo v Česku pouhých 24 tisíc Slováků a na Slovensku ani ne sedm tisíc Čechů. Slovenskou menšinu navíc početně rychle přerostla ukrajinská a málem i vietnamská.

Od té doby ovšem Slováků vytrvale přibývá. Loni jich v Česku žilo 107 tisíc a pokud se trend nezmění, nejpozději za rok budou nejsilnější tuzemskou menšinou.

<wide><div id="mensiny" style="width:100%; height:600px"></div></wide>

*Graf ukazuje cizince, kteří mají v Česku trvalé nebo obvyklé bydliště, zároveň ale mají státní příslušnost jiného státu. Přihlásit se k české nebo slovenské státní příslušnosti sice československé zákony umožnily už v roce 1968, do rozdělení federace v roce 1993 ale měly minimální využití.*

*Historii česko-slovenských vztahů lépe ilustruje národnostní nebo jazyková menšina – občané Československa, kteří uvádějí slovenskou národnost nebo mluví slovensky. Česká a slovenská národnost se sleduje od založení Československa, v [grafickém kvízu](https://www.irozhlas.cz/veda-technologie/historie/kviz-vite-kdo-vydelal-na-rozdeleni-ceskoslovenska_1705300850_dp#content) proto ukazujeme právě národnost. Ze stejného důvodu ovšem data o národnosti příliš nevypovídají o současném vztahu obou států – řada zejména starších lidí na českém území se hlásí ke slovenské národnosti navzdory tomu, že větší část života strávili v západní částí federace, v roce 1993 si zvolili českou státní příslušnost a je tedy přirozenější považovat je za Čechy.*

V článku proto používáme data o státní příslušnosti. Pro ilustraci: při posledním českém sčítání obyvatel v roce 2011 zatrhlo

* 82 tisíc lidí (0,8 % obyvatel Česka) slovenské státní občanství,
* 147 tisíc lidí (1,4 %) slovenskou národnost,
* 154 tisíc lidí (1,5 %) slovenský mateřský jazyk a
* 290 tisíc lidí (2,8 %) místo narození na Slovensku.

Jsou mezi nimi i ti, kteří v Česku žijí pouze přechodně, například studenti. Poslední sčítání obyvatel se totiž týkalo všech lidí s aktuálním pobytem na území Česka ([definice ČSÚ](https://www.czso.cz/csu/sldb/nejcastejsi-dotazy?p_p_id=sucrrisfaqweb_WAR_faqportlet&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-2&p_p_col_count=3&p_p_col_pos=1&_sucrrisfaqweb_WAR_faqportlet__facesViewIdRender=%2Fviews%2FDetail.xhtml&_sucrrisfaqweb_WAR_faqportlet_faqId=112)); dřívější sčítání počítala s trvalým pobytem.

## Mladí muži s mobilními telefony

Slováci jsou oproti většinovým Čechům mladší (průměrně 33,1 vs. 41 let) a je mezi nimi více mužů (53,7 % vs. českých 49,1 %). Jsou také častěji svobodní (52 % vs. 40 %) a mají vyšší vzdělání (19,7 % vs. 12,5 % vysokoškoláků). Podle [analýzy ČSÚ](https://www.czso.cz/documents/10180/20540445/170219-14.pdf/1eae07fa-58cc-4ea0-a0b3-4646c1d8a8dc?version=1.0) data „jednoznačně potvrzují skutečnost, že imigrace občanů Slovenska má pracovní nebo studijní důvody“.

Následující grafy srovnávají slovenskou menšinu v Česku s českou menšinou na Slovensku. Kromě zmíněných demografických rozdílů říkají ještě to, že v roce 2011, kdy v obou zemích proběhlo sčítání obyvatel, byla česká diaspora na Slovensku desetkrát menší.

<wide><div id="demosk" style="width:100%; height:400px"></div></wide>
<wide><div id="democr" style="width:100%; height:400px"></div></wide>

## Za prací do zdravotnictví a továren na autodíly

Data ze sčítání obyvatel ukazují, kde Slováci žijí. Velikost kruhu odpovídá jejich počtu, sytost barvy podílu v obci.

<wide><img src="https://samizdat.cz/data/slovaci-v-cesku/media/sp.png" width="100%"></wide>

Mapa podle státního příslušenství odpovídá účelu jejich pobytu v Česku – práci nebo studiu. Polovina českých Slováků žije podle zmíněné analýzy ČSÚ v Praze, Brně a Plzni.

Vedle největších měst ale žije silná slovenská menšina – téměř šestiprocentní – třeba v Mladé Boleslavi. Nejčastější uplatnění totiž Slováci nacházejí vedle zdravotnictví také v automobilovém průmyslu. Ze stejného důvodu je slovenská diaspora taky v Pardubicích; průmyslová zóna v nedalekých Čívicích se soustředí na výrobu autodílů.

Naopak Brno se v mapě ztrácí, přestože se v něm soustředí většina slovenských studentů. Důvodem může být metodika sběru dat; statistici sice měli ambici zahrnout do sčítání obyvatel i ty, kteří mají v Česku jen přechodné bydliště, ale studenti mohli ze sčítání z různých důvodů vypadnout.

Druhá mapa ukazuje, kde v Česku žije slovenská národnostní menšina – oproti předchozí mapě jsou v ní navíc například ti, kteří se narodili nebo strávili část života na Slovensku a hlásí se ke slovenské národnosti, ale dlouhodobě žijí v Česku a jsou českými státními příslušníky.

<wide><img src="https://samizdat.cz/data/slovaci-v-cesku/media/n.png" width="100%"></wide>

Oproti předchozí mapě přibyl pás obcí podél severní a východní hranice. Jde zřejmě o výsledek dosídlování českého pohraničí po odsunu tamějších Němců po druhé světové válce, jak ukázala [naše analýza demografie Sudet](https://samizdat.cz/data/sudety-clanek/www/).

Právník Aleš Ziegler z vládní Agentury pro sociální začleňování upozorňuje, že může jít o potomky přesídlených slovenských Romů. Ti v důsledku komplikovaných pravidel pro nabytí českého nebo slovenského občanství získali v roce 1968 to slovenské, přestože žili na českém území. Většina z nich to zjistila až po rozdělení federace v roce 1993. Důsledkem je – vedle silné slovenské menšiny na mapě – složitý přístup ke vzdělání, zdravotní péči, sociálním dávkám i důchodům, jak v detailu popisuje [vládní dokument](https://www.vlada.cz/cz/ppov/zalezitosti-romske-komunity/dopady-pravnich-predpisu-5753/).

## Brněnská informatika: Slováci jsou v převaze

Baštou slovenských studentů je Brno. Nejvyšší podíl Slováků, přes 17 procent, má Janáčkova akademie múzických umění, hned po ní Masarykova univerzita. Ostatně v první pětici škol podle podílu slovenských studentů jsou pouze brněnské univerzity.

<wide><div id="univerzity" style="width:100%; height:400px"></div></wide>

Studenti ze Slovenska můžou být pro univerzity komplikace: stát na ně přispívá méně než na ty české. [Rozhovor s rektorem Masarykovy univerzity](https://www.irozhlas.cz/zpravy-domov/rektor-mu-socialisticka-vlada-brani-v-pristupu-ke-vzdelani-chce-si-zajistit-volice_201512080600_jbocek) před dvěma lety ovšem ukázal, že Slováci můžou být pro školu prestižní.

„Jsme připraveni smířit se s tím, že za Slováky dostaneme o něco méně, protože jsou to lepší studenti. Z hlediska kvality studia jsou pro univerzitu přínosem,“ vysvětlil tehdejší i dnešní rektor Mikuláš Bek. „Slováci mají silnou motivaci jít studovat k nám, na Slovensku je s českými kamennými univerzitami srovnatelně kvalitní jenom Univerzita Komenského.“

Na stejné univerzitě je taky jediná česká fakulta, kde je Slováků víc než domácích: Fakulta informatiky. Vedle 982 Slováků zde loni studovalo 943 Čechů, 30 Indů a 11 Ukrajinců. Právě informatice se slovenští muži v Česku věnují nejčastěji. Slovenky zase typicky studují lékařství nebo farmacii.

<wide><div id="fakulty" style="width:100%; height:400px"></div></wide>